import java.util.*; /*Melakukan import paket library dari java util */
public class Task31 { /*Membuat sebuah class dengan nama Task31 */
	public static void main(String []args){ /*Membuat sebuah objek */
		ArrayList<String> list = new ArrayList <String> (); /* Membuat sebuah array list dengan tipe data string */
		list.add("Mango"); /* Memasukkan string Mango ke dalam array list */
		list.add("Apple"); /* Memasukkan string Apple ke dalam array list */
		list.add("Banana"); /* Memasukkan string Banana ke dalam array list */
		list.add("Grapes"); /* Memasukkan string Grapes ke dalam array list */
		Iterator itr = list.iterator(); /*Menggunakan fungsi iterator pada paket library java util dan membuat variabel baru dengan nama itr */
		while(itr.hasNext()) { /* Membuat perulangan dengan kondisi hasnext */
			System.out.println(itr.next()); /* Menampilkan hasil dari perulangan */
		}
	}
}

/* Mengimport paket library dari java/util, lalu membuat sebuah kelas dengan nama Task31, lalu membuat suatu objek.
Kemudian membuat suatu array list dengan tipe data string dan sebuah variabel baru dengan nama list.
Menambahkan nilai ke dalam array list dengan string (Mango, Apple, Banana, Grapes), kemudian membuat suatu fungsi iterator
yang berfungsi untuk menampilkan dan mengakses nilai di array list tersebut dengan nama variabel baru yaitu itr.
Lalu melakukan perulangan dengan while dimana kondisinya menggunakan metode hasNext(), berfungsi untuk mengecek nilai selanjutnya
dari string pada Array list. Lalu tampilkan nilai string dari Array list. Output yang dihasilkan dari potongan program diatas yaitu
Mango, Apple, Banana, Grapes */