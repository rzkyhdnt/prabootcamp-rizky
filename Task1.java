public class Task1{ /* membuat class dengan nama Task1*/
	public static void main(String []args){
		int i = 1; /* mendeklarasikan variabel i bernilai sama dengan 1*/
		do { /* mendeklarasikan perintah lakukan perulangan */
			if (i == 5) { /* jika i bernilai sama dengan 5 */
				i++; /* i melakukan increment */
				break; /* keluar dari looping */
			}
			System.out.println(i); /* tampilkan nilai i */
			i++; /* i dilakukan increment */
		} while(i<=10); /* lakukan perulangan selama i lebih kecil sama dengan 10 */
	}
}

/* Pertama-tama buat sebuah kelas dengan nama Task1.\,
kemudian membuat objek, lalu dibuat suatu variabel dengan nama i bernilai 1,
kemudian dilakukan perulangan menggunakan metode do-while dengan kondisi di dalam do yaitu,
jika i bernilai sama dengan 5, lakukan penambahan pada i dan keluar dari looping,
jika i bernilai tidak sama dengan 5, tampilkan nilai i, dan i dilakukan penambahan (increment),
perulangan dilakukan selama i lebih kecil sama dengan 10.
Output yang dihasilkan adalah 1, 2, 3, 4. yang artinya, ketika i sama dengan 5, kondisi di dalam if(i == 5)
dieksekusi, dan keluar daru perulangan. */