public class Task2{ /* Membuat sebuah class dengan nama Task2 */
	public static void main(String []args){ /* Membuat objek */
		int age = 25; /* Mendeklarasikan variabel dengan nama age dengan nilai sama dengan 25 */
		int weight = 48; /* Mendeklarasikan variable dengan nama weight dengan nilai sama dengan 58 */
		if(age >= 18){ /* Membuat suatu kondisi dimana age lebih besar sama dengan 18 */
			if(weight > 50) { /* Membuat kondisi dimana weight lebih besar dari 50 */
				System.out.println("You are eligible to donate blood"); /* Menampilkan keluaran berupa string dengan kalimat sebagai berikut */
			} else { /* atau */
				System.out.println("You are not eligible to donate blood"); /* Jika kondisi diatas tidak terpenuhi, maka tampilkan string dengan kalimat sbb */
			}
		} else { /* jika kondisi diatas semua tidak terpenuhi */
			System.out.println("Age must be greater than 18"); /* Tampilkan output dengan kalimat string sebagai berikut */
		}
	}
}

/* Membuat sebuah class dengan nama Task2,
Lalu membuat objek, mendeklarasikan tipe data integer dengan nama age dengan nilai 25,
lalu mendeklarasikan tipe data integer dengan nama weight dengan nilai 48,
membuat suatu kondisi dimana jika nilai age lebih besar sama dengan 18, dan jika nilai weight
lebih besar dari 50, maka tampilkan string You are eligible to donate blood, jika age lebih besar sama dengan 18,
dan weight kurang dari 50, maka tampilkan string You are not eligible to donate blood, dan jika age kurang dari 18,
maka tampilkan string dengan kalimat Age must be greater than 18. Karena program diatas nilai age nya 25 dan weightnya 48, 
maka output yg dihasilkan dari potongan kode diatas adalah You are not eligible to donate blood */