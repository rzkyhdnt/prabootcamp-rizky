import java.util.*;
public class HashMapExample1{
	public static void main(String []args){
		HashMap<Integer,String> map = new HashMap<Integer, String>();
		map.put(1, "Mango");
		map.put(2, "Apple");
		map.put(3, "Banana");
		map.put(4, "Grapes");
		map.put("pisang", "Pisang");

		System.out.println("Iterating Hashmap...");
		for(Map.Entry m : map.entrySet()){
			System.out.println(m.getKey()+" "+m.getValue());
		}
	}
}

/* Mengimport paket library dari java/util
Mengeksekusi objek, lalu membuat sebuah HashMap dengan kata kunci integer dan nilainya string dengan nama variabel map,
lalu membuat key hashmap 1 dengan nilai Mango, key hashmap 2 dengan nilai Apple, key hashmap 3 dengan nilai Banana, key hashmap
4 dengan nilai Grapes dan key hashmap pisang dengan nilai pisang. Output pada potongan diatas seharusnya menampilkan isi dari
key dan valuenya, namun program error dikarenakan tipe data pada key "pisang" berbeda dengan tipe data hashmap yang dideklarasikan 
yaitu integer bukanlah string. Maka dari program diatas terjadi error. */