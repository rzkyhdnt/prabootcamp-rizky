import java.util.*; /* Mengimport paket library java.util */
public class Task32{ /* Membuat class dengan nama Task32 */
	public static void main(String []args){ /* Mengeksekusi objek */
		ArrayList<String> list = new ArrayList<String>(); /* Membuat array list dengan nama variabel list dengan nilai dengan tipe data string */
		list.add("Mango"); /* Menambahkan nilai string Mango ke dalam array list */
		list.add("Apple"); /* Menambahkan nilai string Apple ke dalam array list */
		list.add("Banana"); /* Menambahkan nilai string Banana ke dalam array list */
		list.add("Grapes"); /* Menambahkan nilai string Grapes ke dalam array list */

		System.out.println(list); /* Menampilkan niali dari variabel list */
	}
}

/* Mengimport paket library java/util/*(semua paket di dalam folder java/util)
membuat kelas dengan nama Task32, kemudian mengeksekusi objek. Lalu buat suatu array list dengan tipe data string, kemudian mendeklarasikan
variabel baru dengan nama list berisi objek baru. kemudian menambahkan nilai mango, apple, banana, dan grapes ke dalam array list.
Kemudian menampilkan nilai yg berada di array list. Output yang dihasilkan adalah ["Mango", "Apple", "Banana", "Grapes"] */